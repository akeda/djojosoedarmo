<?php
class AppController extends Controller {
    var $components = array(
        'Auth', 'Session', 'Cookie'
    );
    var $helpers       = array('Html', 'Form', 'Javascript', 'Time', 'Session');
    
    function beforeFilter() {
        $navs = array(
            array(
                'label' => 'Beranda', 'controller' => 'pages', 'action' => 'home', 'class' => 'home'
            ),
            array(
                'label' => 'Prakata', 'controller' => 'pages', 'action' => 'intro', 'class' => 'intro'
            ),
            array(
                'label' => 'Daftar keluarga', 'controller' => 'families', 'action' => 'index', 'class' => 'list'
            ),
            array(
                'label' => 'Arsip Berita', 'controller' => 'posts', 'action' => 'index', 'class' => 'archive'
            )
        );
        extract($this->__getControllerAction());
        
        $this->Auth->authorize = 'controller';
        $this->Auth->loginError = 'Username/password salah';
        $this->Auth->authError = 'Anda perlu login';
        $this->Auth->authorize = 'controller';
        $this->Auth->flashElement = "error";
        
        $isAdmin = false;
        $showProfile = false;
        $profile = array();
        if ( $this->Auth->user('id') ) {
            $showProfile = true;
            $profile = $this->Auth->user();
            if ( $this->Auth->user('type') == 'admin' ) {
                $isAdmin = true;
            }
        }
        if ( $this->__isPublic() ) {
            $this->Auth->allow($action);
        }
        
        // images related
        $this->pathPrefix  = WWW_ROOT . 'files' . DS . 'family_photos';
        $this->pathImage = $this->pathPrefix . DS;
        $this->urlImage = $this->webroot . 'files/family_photos/';
        $this->fieldImage = 'imagename';
        
        // set prefix
        $this->resizedPrefix = 'thumb_';
        $this->thumbPrefix = 'thumbsmall_';
        $this->iconPrefix = 'icon_';
        // images related
        $this->set('urlImage', $this->urlImage);
        $this->set('resizedPrefix', $this->resizedPrefix);
        $this->set('thumbPrefix', $this->thumbPrefix);
        
        $this->set(compact('navs', 'controller', 'action', 'showProfile', 'profile', 'isAdmin'));
    }
    
    function isAuthorized() {
        if ( $this->Auth->user('type') == 'admin' ) {
            return true;
        }
        
        return $this->__isPublic();
    }
    
    function __getControllerAction() {
        $controller = $this->params['controller'];
        $this->controller = $controller;
        $action = $this->params['action'];
        $this->action = $action;
        
        return array('controller' => $controller, 'action' => $action);
    }
    
    function __isPublic() {
        $publics = array(
            array('pages' => 'intro'),
            array('pages' => 'home'),
            array('families' => 'index'),
            array('families' => 'getFeatured'),
            array('families' => 'getBirthday')
        );
        
        extract($this->__getControllerAction());
        foreach ($publics as $public) {
            if ( isset($public[$controller]) && $public[$controller] == $action ) {
                return true;
            }
        }
        return false;
    }
    
    function __pathToController() {
        $plugin = isset($this->params['plugin']) ? $this->params['plugin'] . '/' : '';
        return $this->webroot . $plugin . $this->params['controller'];
    }
    
    function __resizeImage($filename) {
        $image_info = getimagesize($this->pathImage . $filename);
        $image_type = $image_info[2];
        
        // create tmp image
        if ( $image_type == IMAGETYPE_JPEG ) {
            $image_tmp = imagecreatefromjpeg($this->pathImage . $filename);
        } else if ( $image_type == IMAGETYPE_GIF ) {
            $image_tmp = imagecreatefromgif($this->pathImage . $filename);
        } else if ( $image_type == IMAGETYPE_PNG ) {
            $image_tmp = imagecreatefrompng($this->pathImage . $filename);
        } else {
            $this->errorUpload = 'Unsupported image type';
            
            return false;
        }
        
        $width = imagesx($image_tmp);
        $height = imagesy($image_tmp);
        
        // scaling based on width
        if ( $width >= 1500 ) {
            $scale = 25;
        } else if ( $width < 1500 ) {
            $scale = 50;
        } else {
            $this->errorUpload = 'Image is too large';   
            return false;
        }
        // resizing
        $resized_width = $width * $scale / 100; 
        $resized_height = $height * $scale / 100;
        
        // create resized version
        $this->__createThumb($image_tmp, array(
            'image_type' => $image_type, 'filename' => $filename, 'prefix' => $this->resizedPrefix,
            'x' => 0, 'y' => 0, 'resized_width' => $resized_width, 'resized_height' => $resized_height,
            'x_crop' => $width, 'y_crop' => $height
        ));
        
        // create small thumb 100x100
        $this->__createThumb($image_tmp, array(
            'image_type' => $image_type, 'filename' => $filename, 'prefix' => $this->thumbPrefix,
            'resized_width' => 180, 'resized_height' => 180
        ));
        
        // create icon 25, 25
        $this->__createThumb($image_tmp, array(
            'image_type' => $image_type, 'filename' => $filename, 'prefix' => $this->iconPrefix,
            'resized_width' => 25, 'resized_height' => 25
        ));
        
        return true;
    }
    
    function __createThumb($res, $params = array()) {
        $default = array(
            'image_type' => IMAGETYPE_JPEG, 'filename' => 'default_resized.jpeg',
            'resized_width' => 100, 'resized_height' => 100, 'prefix' => 'thumb_'
        );
        $params = array_merge($default, $params);
        
        $width = imagesx($res);
        $height = imagesy($res);
        $long = ($width > $height) ? $width : $height;
        $crop = $long / 2;
        $x = ($width - $crop) / 2;
        $y = ($height - $crop) / 2;
        if ( !isset($params['x']) ) $params['x'] = $x;
        if ( !isset($params['y']) ) $params['y'] = $y;
        if ( !isset($params['x_crop']) ) $params['x_crop'] = $crop;
        if ( !isset($params['y_crop']) ) $params['y_crop'] = $crop;

        $thumb = imagecreatetruecolor($params['resized_width'], $params['resized_height']);
        imagecopyresampled($thumb, $res, 0, 0, $params['x'], $params['y'],
                           $params['resized_width'], $params['resized_height'],
                           $params['x_crop'], $params['y_crop']);
        
        // save thumb
        if ( $params['image_type'] == IMAGETYPE_JPEG ) {
            imagejpeg($thumb, $this->pathImage . $params['prefix'] . $params['filename'], 75);
        } else if ( $params['image_type'] == IMAGETYPE_GIF ) {
            imagegif($thumb, $this->pathImage . $params['prefix'] . $params['filename']);
        } else if ( $params['image_type'] == IMAGETYPE_PNG ) {
            imagepng($thumb, $this->pathImage . $params['prefix'] . $params['filename']);
        }
        
        return true;
    }
}
