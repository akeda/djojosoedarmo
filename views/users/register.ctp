<?php $html->css('admin', 'stylesheet', array('inline' => false));?>
<div class="span-24 last">
    <div class="box-content-outer">
        <div class="box-content">
            <?php echo $this->Form->create('User');?>
                <fieldset>
                    <legend>Registrasi</legend>
                <?php
                    echo $this->Form->input('name');
                    echo $this->Form->input('password');
                    echo $this->Form->input('password_confirm', array(
                        'label' => 'Konfirmasi password', 'required' => true, 'type' => 'password',
                        'div' => array(
                            'class' => 'input password required'
                        )
                    ));
                    echo $this->Form->input('email');
                ?>
                </fieldset>
            <?php echo $this->Form->end(__('Register', true));?>
        </div>
    </div>
</div>
