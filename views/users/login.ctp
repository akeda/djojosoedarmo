<?php $html->css('admin', 'stylesheet', array('inline' => false));?>
<div class="span-24 last">
    <div class="box-content-outer">
        <div class="box-content">
            <?php echo $this->Form->create('User');?>
            <fieldset>
                <legend>Login</legend>
            <?php
                echo $this->Form->input('email');
                echo $this->Form->input('password');
                echo $this->Form->submit('Login', array('div' => false));
                echo ' &nbsp; Belum punya account? ';
                echo $this->Html->link('Register', array('action' => 'register'));
            ?>
            </fieldset>
        </div>
    </div>
</div>
