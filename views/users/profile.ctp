<?php $html->css('admin', 'stylesheet', array('inline' => false));?>
<div class="span-24 last">
    <div class="box-content-outer">
        <div class="box-content">
        <?php echo $this->Form->create('User');?>
            <fieldset>
                <legend>Ganti password</legend>
            <?php
                echo $this->Form->input('password', array(
                    'value' => '', 'label' => 'Password baru'
                ));
                echo $this->Form->input('password_confirm', array(
                    'label' => 'Konfirmasi password baru', 'required' => true, 'type' => 'password',
                    'div' => array(
                        'class' => 'input password required'
                    )
                ));
            ?>
            </fieldset>
        <?php echo $this->Form->end(__('Update', true));?>
        </div>
    </div>
</div>
