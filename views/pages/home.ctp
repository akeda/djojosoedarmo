<?php echo $html->css('http://code.google.com/apis/maps/documentation/javascript/examples/standard.css', 'stylesheet', array('inline' => false));?>
<?php $html->script('http://maps.google.com/maps/api/js?sensor=false', array('inline' => false));?>
<?php $html->script('markerclusterer_compiled', array('inline' => false));?>
<style type="text/css">
#map_canvas {
    width: 100%;
    height: 300px;
}
</style>
<div class="span-24 last" id="splash_container">
    <!-- birthday box -->
    <div class="span-16">
        <div id="splash"></div>
        <div id="todaysbirthday" class="box-content-outer">
            <div class="box-content common">
                <div id="todaysbirthday-content">
                    <span class="label">Yang berulang tahun hari ini :</span>
                    <span class="val">
                        <?php echo $this->element('birthday');?>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <!-- featured box -->
    <div class="span-7 prepend-1 clearfix last">
        <?php echo $this->element('featured');?>
    </div>
</div>

<div class="span-24 last" id="content-container">
    <!-- posts box-->
    <div class="span-11 append-1" id="news-container">
        <div id="news-content" class="box-content-outer">
            <div class="box-content">
                <h2>Berita Keluarga</h2>
                <ul>
                    <li>
                        <a href="#">Ulang Tahun ke-27 Akeda Bagus</a> &nbsp;
                        <span class="date">13 Juli 2010 21:00</span><br />
                        <span class="label">Ditulis oleh</span> <span class="val">Purnomo Sujadi</span>
                    </li>
                    <li>
                        <a href="#">Berita Duka Meninggal Dunia</a> &nbsp;
                        <span class="date">12 Juli 2010 21:00</span><br />
                        <span class="label">Ditulis oleh</span> <span class="val">Purnomo Sujadi</span>
                    </li>
                </ul>
                <p class="more">
                    <a href="#">Lihat berita lainnya</a> &raquo;
                </p>
            </div>
        </div>
    </div>
    <!-- map cluster box-->
    <div class="span-12 last" id="get-in-touch-container">
        <div id="get-in-touch" class="box-content-outer">
            <div id="get-in-touch" class="box-content common">
                <h2>Informasi Lebih Lanjut</h2>
                <p>
                    Situs ini dikelola oleh <span class="alter">H. Djoko Mulyono, MBA</span>,
                    <span class="alter">Purnomo Sujadi</span> dan <span class="alter">Akeda Bagus</span>. Jika ada saran, masukan dan
                    kritik, silahkan isi form di <a href="#">halaman ini</a>. Kami sangat mengharapkan masukan ini demi perbaikan situs
                    keluarga <span class="alter">Djojosoedarmo</span>.
                </p>
            </div>
        </div>
    </div>
    
</div>

<div class="span-24 last" style="margin-top: 20px;">
    <div class="box-content-outer">
        <div class="box-content">
            <div id="map_canvas_container">
                <h2>Peta sebaran keluarga Djojosoedarmo</h2>
                <div id="map_canvas"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var data = <?php echo $families;?>;
var urlImage = "<?php echo $urlImage;?>";
var thumbPrefix = "<?php echo $thumbPrefix;?>";
var map;
$(function() {
    function init() {
        // Kp. Melayu
        var center = new google.maps.LatLng(0.087891,122.783203);
        
        map = new google.maps.Map(document.getElementById("map_canvas"), {
            zoom: 4,
            center: center,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        
        var markers = [];
        for (var i=0, dataFamily; dataFamily = data[i]; i++) {
            if ( dataFamily['Family']['lat'] !== null && dataFamily['Family']['lng'] !== null ) {
                var latlng = new google.maps.LatLng(dataFamily['Family']['lat'], dataFamily['Family']['lng']);
                var marker = new google.maps.Marker({
                    position: latlng,
                    title: dataFamily['Family']['fullname']
                });
                var filename = '<?php echo $html->image('thumb_no_photo.jpg', array('width' => 75, 'height' => 75))?>';
                if ( dataFamily['Family']['filename'] ) {
                    filename = '<img src="' + urlImage + thumbPrefix + dataFamily['Family']['filename'] +
                               '" width="75" height="75" />';
                }
                var info = '<h3>' + dataFamily['Family']['fullname'] + '</h3>' +
                           '<div style="float: left; margin-right: 10px;">' + filename + '</div>' +
                           '<p>' + dataFamily['Family']['address'] + '</p>';
                var infowindow = new google.maps.InfoWindow({
                    content: info,
                    size: new google.maps.Size(100,200)
                });
                var fn = markerClicked(marker, infowindow);
                google.maps.event.addListener(marker, 'click', fn);
                markers.push(marker);
            }
        }
        var markerCluster = new MarkerClusterer(map, markers);
    }
    function markerClicked(m, iw) {
        return function() {
            google.maps.event.addListener(m, 'click', function() {
                iw.open(map, m);
            });
        }
    }
    init();
});
</script>
