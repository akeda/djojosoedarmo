<div class="span-24 last" id="splash_container">
    <!-- birthday box -->
    <div class="span-16">
        <div id="splash"></div>
        <div id="todaysbirthday" class="box-content-outer">
            <div class="box-content common">
                <div id="todaysbirthday-content">
                    <span class="label">Yang berulang tahun hari ini :</span>
                    <span class="val">
                        <?php echo $this->element('birthday');?>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <!-- featured box -->
    <div class="span-7 prepend-1 clearfix last">
        <?php echo $this->element('featured');?>
    </div>
</div>

<div class="span-24 last" id="content-container">
    <!-- posts box-->
    <div class="span-11 append-1" id="news-container">
        <div id="news-content" class="box-content-outer">
            <div class="box-content">
                <h2>Berita Keluarga</h2>
                <ul>
                    <li>
                        <a href="#">Ulang Tahun ke-27 Akeda Bagus</a> &nbsp;
                        <span class="date">13 Juli 2010 21:00</span><br />
                        <span class="label">Ditulis oleh</span> <span class="val">Purnomo Sujadi</span>
                    </li>
                    <li>
                        <a href="#">Berita Duka Meninggal Dunia</a> &nbsp;
                        <span class="date">12 Juli 2010 21:00</span><br />
                        <span class="label">Ditulis oleh</span> <span class="val">Purnomo Sujadi</span>
                    </li>
                </ul>
                <p class="more">
                    <a href="#">Lihat berita lainnya</a> &raquo;
                </p>
            </div>
        </div>
    </div>
    <!-- map cluster box-->
    <div class="span-12 last" id="get-in-touch-container">
        <div id="get-in-touch" class="box-content-outer">
            <div id="get-in-touch" class="box-content common">
                <h2>Informasi Lebih Lanjut</h2>
                <p>
                    Situs ini dikelola oleh <span class="alter">H. Djoko Mulyono, MBA</span>,
                    <span class="alter">Purnomo Sujadi</span> dan <span class="alter">Akeda Bagus</span>. Jika ada saran, masukan dan
                    kritik, silahkan isi form di <a href="#">halaman ini</a>. Kami sangat mengharapkan masukan ini demi perbaikan situs
                    keluarga <span class="alter">Djojosoedarmo</span>.
                </p>
            </div>
        </div>
    </div>
    
</div>
