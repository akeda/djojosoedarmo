<?php 
    $records = $this->requestAction(
        array(
            'controller' => 'families', 'action' => 'getBirthday'
        )
    );
    
    $bd = array();
    foreach ($records as $id => $record):
        $bd[] = $html->link($record, array('controller' => 'families', 'action' => 'view', $id));
    endforeach;
    
    if ( empty($bd) ) {
        echo 'Tidak ada yang berulang tahun hari ini';
    } else {
        echo implode(', ', $bd);
    }
?>
