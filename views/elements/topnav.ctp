<ul>
    <?php foreach ($navs as $nav):?>
        <li>
        <?php echo $html->link($nav['label'],
                array('controller' => $nav['controller'], 'action' => $nav['action']),
                array('class' => $nav['class'] . (
                    ($nav['controller'] == $controller && $nav['action'] == $action) ?
                    ' selected' : '')
                )
            );
        ?>
        </li>
    <?php endforeach;?>
</ul>
<div id="nav-top-search">
    <input type="text" id="nav-top-search-field" />
</div>
