<div class="box-content-outer" id="featured-member-container">
    <div class="box-content common" id="featured-member">
        <h2>Anggota Keluarga</h2>
        <?php 
            $records = $this->requestAction(
                array(
                    'controller' => 'families', 'action' => 'getFeatured'
                )
            );
            
            foreach ($records as $key => $record):
                $image = $html->image('small_thumb_no_photo.jpg');
                if ( !empty($record['Family']['filename']) ) {
                    $image = '<img src="' . $urlImage . $thumbPrefix . $record['Family']['filename'] . 
                             '" width="75" height="75" />';
                }
                $name = $record['Family']['fullname'];
                $imagelinks = '<div class="fmemberphoto">' . $image . '</div>' .
                              '<span class="fmembername">' . $name . '</span>';

                $content = '<div class="fmember_row">' .
                            $html->link($imagelinks, array(
                                'controller' => 'families', 'action' => 'view', $record['Family']['id']
                            ), array('escape' => false)) .
                            '</div>';

                if ($key % 3 == 0 && $key > 0) {
                    echo '</div>';
                }
                if ($key % 3 == 0) {
                    echo '<div class="fmember clearfix">';
                }
                echo $content;
            endforeach;
        ?>
        </div>
        <div class="fmember_other">
            <?php
                echo $html->link('Lihat anggota lainnya &raquo;', array(
                    'controller' => 'families', 'action' => 'index'
                ), array('escape' => false));
            ?>
        </div>
    </div>
</div>
