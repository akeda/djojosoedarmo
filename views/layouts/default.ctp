<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <title>Situs Keluarga Besar Djojosoedarmo</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <?php
        echo $html->meta('icon');
        echo $html->css('blueprint/screen', 'stylesheet', array('media' => 'screen, projection'));
        echo $html->css('blueprint/print', 'stylesheet', array('media' => 'print'));
    ?>
    <!--[if lt IE 8]>
    <?php
        echo $html->css('blueprint/ie', 'stylesheet', array('media' => 'screen, projection'));
    ?>
    <![endif]-->
    <?php
        echo $html->css('style?20110119', 'stylesheet', array('media' => 'screen, projection'));
        echo $html->script('jquery');
        echo $scripts_for_layout;
    ?>
</head>
<body>
<div class="container">
    <div class="span-24 last" id="hd">
        <div class="span-10" id="hd-logo">
            <?php echo $html->image('logo');?>
        </div>
        <div class="span-14 clearfix last" id="nav-top">
            <?php echo $this->element('topnav');?>
        </div>
    </div>
    
    <div class="span-24 last">
        <div id="hdflash" role="banner">
        <?php
            if ($session->check('Message.flash')):
                echo $session->flash();
            endif;
            echo $this->Session->flash('auth');
        ?>
    </div>
    </div>
    
    <?php echo $content_for_layout;?>
    <div class="span-24 last" id="ft">
        &copy; 2010 Djojosoedarmo. All rights reserved.<br />
        Phone: +62 813 15528932.<br />
        Jakarta, Indonesia<br />
    </div>
    
    <div id="usernav" class="box-content-outer">
        <div id="usernav-wrap" class="box-content">
            <ul>
                <?php if ($showProfile): // logged ?>
                <li><div style="margin-bottom: 10px; color: #fff">Hi, <?php echo $profile['User']['name'];?></div></li>
                <li>
                <?php
                    echo $html->link('Tambah data keluarga', array(
                        'controller' => 'families', 'action' => 'add'
                    ));
                ?>
                </li>
                <li>
                <?php
                    echo $html->link('Tambah user', array(
                        'controller' => 'users', 'action' => 'logout'
                    ));
                ?>
                </li>
                <li>
                <?php
                    echo $html->link('Logout', array(
                        'controller' => 'users', 'action' => 'logout'
                    ));
                ?>
                </li>
                <li>
                <?php
                    echo $html->link('Ganti password', array(
                        'controller' => 'users', 'action' => 'profile'
                    ));
                ?>
                </li>
                <?php else:?>
                <li>
                <?php
                    echo $html->link('Login', array(
                        'controller' => 'users', 'action' => 'login'
                    ));
                ?>
                </li>
                <?php endif;?>
            </ul>
        </div>
    </div>
</div>
</body>
</html>
