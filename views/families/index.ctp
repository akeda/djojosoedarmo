<?php $html->css('list', 'stylesheet', array('inline' => false));?>
<div class="span-24 last" id="searchmember">
    <div class="searchmember_container clearfix">
        <form accept-charset="UNKNOWN" enctype="application/x-www-form-urlencoded"
              method="get" name="search-form" id="search-form">
            <span class="label">Cari Berdasar Nama</span>
            <span class="val">
                <input type="text" name="fullname" class="searchfield" id="searchfield"
                       value="<?php echo $fullname;?>" />
            </span>
        </form>
    </div>
</div>

<div class="span-24 last">
    <div id="alphabet-list" class="clearfix">
        <?php
            foreach ($alphabets as $alphabet):
                echo $html->link($alphabet, array(
                    'action' => 'index', '?alphabet=' . $alphabet
                ), array(
                    'class' => $current_alphabet == $alphabet ? 'selected' : ''
                ));
            endforeach;
        ?>
    </div>
    <div id="list-result">
    <?php if ( !empty($fullname) ):?>
        Hasil pencarian untuk <strong><?php echo $fullname;?></strong>
    <?php else:?>
        Daftar Biodata Keluarga Berawalan <strong><?php echo $current_alphabet;?></strong>
    <?php endif;?>
    <?php echo count($records) ? ', ada ' . count($records) . ' anggota' : '';?>
    </div>    
    <div id="people-list" class="box-content-outer">
        <ul class="box-content" id="people-list-content">
            <?php
                if (!empty($records)): 
                $i = 0;
            ?>
            <?php foreach ($records as $key => $record):?>
                <li <?php echo ++$i%2==0?' class="alt"':''; ?>>
                    <div class="people-pict">
                    <?php if ( !empty($record['Family']['filename']) ):?>
                        <?php 
                            echo $html->link('<img src="' . $urlImage . $thumbPrefix . $record['Family']['filename'] . 
                                             '" width="100" height="100" />',
                                        array('action' => 'view', $record['Family']['id']),
                                        array('escape' => false)
                                );
                        ?>
                    <?php else:?>
                        <?php 
                            echo $html->link($html->image('thumb_no_photo.jpg'), array(
                                'action' => 'view', $record['Family']['id'],
                            ), array('escape' => false));
                        ?>
                    <?php endif;?>
                    </div>
                    <div class="people-desc">
                        <h3>
                        <?php
                            echo $html->link($record['Family']['fullname'], array(
                                'action' => 'view', $record['Family']['id']
                            ));
                        ?>
                        </h3>
                        <p>
                        <?php
                            // birth
                            echo 'Lahir di ' . $record['Family']['birthplace'] . ' pada tanggal ' .
                                  $time->format('d/m/Y', $record['Family']['birthday']) . '. ';
                            
                            // work
                            if ( !empty($record['Family']['work']) ):
                                echo 'Pekerjaan terakhir adalah ' . $record['Family']['work'] . '. ';
                            endif;
                            
                            // couple
                            if ( $record['Family']['status'] == 'M' ):
                                echo 'Menikah dengan ' . $record['Family']['couple_name'] . ', ';
                                echo $record['Family']['couple_work'] . ', pada tanggal ';
                                echo $time->format('d/m/Y', $record['Family']['married_date']) . '.';
                            endif;
                            
                            // children
                            
                            
                            // work
                        ?>
                        </p>
                        <div class="people-desc-detail">
                            <div class="label">Agama:</div>
                            <div class="val">
                                <?php
                                    echo isset($religions[$record['Family']['religion']]) ?
                                         $religions[$record['Family']['religion']] : '';
                                ?>
                            </div>
                        </div>
                        <div class="people-desc-detail">
                            <div class="label">Alamat:</div>
                            <div class="val">
                                <?php echo $record['Family']['phone'];?>
                            </div>
                        </div>
                        <?php if ($isAdmin):?>
                        <div class="modifylinks">
                            <span class="modiy">
                            <?php
                                echo $html->link('Edit', array(
                                    'action' => 'edit', $record['Family']['id']
                                ), array('escape' => false, 'class' => 'edit'));
                            ?>
                            </span>
                            <span class="modiy">
                            <?php
                                echo $html->link('Hapus',
                                    array('action'=>'delete', $record['Family']['id']),
                                    array('class' => 'delete'),
                                    sprintf(__('Yakin ingin menghapus data ', true) . ' %s?',
                                        $record['Family']['fullname'])
                                );
                            ?>
                            </span>
                        </div>
                        <?php endif;?>
                    </div>
                </li>
            <?php endforeach;?>
            <?php else: ?>
                <li>Tidak ada data</li>
            <?php endif;?>
        </ul>
    </div>
</div>
<script type="text/javascript">
$(function() {    
    $('#searchfield').blur(function() {
        if ( !this.value.length ) {
            this.value = '<?php echo $searchfield_default;?>';
            $(this).css({'color': '#999'});
        } else {
            $(this).css({'color': '#666'});
        }
    }).click(function() {
        if ( this.value == '<?php echo $searchfield_default;?>' ) {
            this.value = '';
        }
    }).trigger('blur');
});
</script>
