<?php $html->css('admin', 'stylesheet', array('inline' => false));?>
<h2><?php echo $title . ' ' . $this->data['Family']['fullname'];?></h2>
<div class="span-24 last">
    <div class="box-content-outer">
        <div class="box-content">
        <?php echo $form->create('Family'); ?>
        <table>
            <tr>
                <td>
                <?php
                    echo $form->input('fullname', array('label' => 'Nama lengkap'));
                    echo $form->input('featured', array(
                        'label' => 'Tampilkan di halaman depan', 'type' => 'checkbox'
                    ));
                    echo $form->input('parent_id', array(
                        'type' => 'select', 'options' => $families, 'empty' => true, 'label' => 'Orang tua'
                    ));
                    echo $form->input('birthday',
                        array('dateFormat' => 'DMY', 'label' => 'Tgl. Lahir', 'minYear' => 1893, 'maxYear' => date('Y'))
                    );
                    echo $form->input('birthplace', array('label' => 'Tempat Lahir'));
                    echo $form->input('rip_date',
                        array('dateFormat' => 'DMY', 'label' => 'Tgl. Wafat', 'minYear' => 1893, 'maxYear' => date('Y'))
                    );
                    echo $form->input('rip_place', array('label' => 'Tempat Wafat'));
                    
                    echo $form->input('religion', array(
                        'type' => 'select', 'options' => $religions, 'empty' => true, 'label' => 'Agama'
                    ));
                    echo $form->input('work', array('label' => 'Pekerjaan terakhir'));
                    
                    echo $form->radio('gender', array('M' => 'Laki-laki', 'F' => 'Perempuan'),
                        array('legend' => 'Jenis Kelamin')
                    );
                    echo $form->input('phone', array('label' => 'Nomor telp'));
                    echo $form->input('email', array('label' => 'Email'));
                    echo $form->input('address', array('label' => 'Alamat', 'type' => 'textarea', 'rows' => 5));
                ?>
                </td>
                <td>
                <?php
                    echo $form->radio('status', array('S' => 'Sendiri', 'M' => 'Menikah', 'D' => 'Cerai'));
                    // couple
                    echo $form->input('married_date',
                        array('dateFormat' => 'DMY', 'label' => 'Tgl. Menikah', 'minYear' => 1893, 'maxYear' => date('Y'))
                    );
                    echo '<fieldset>';
                    echo '<legend>Data suami/istri</legend>';
                    echo $form->input('couple_name', array('label' => 'Nama istri/suami'));
                    echo $form->input('couple_birthday',
                        array('dateFormat' => 'DMY', 'label' => 'Tgl lahir istri/suami', 'minYear' => 1893, 'maxYear' => date('Y'))
                    );
                    echo $form->input('couple_birthplace', array('label' => 'Tempat lahir istri/suami'));
                    echo $form->input('couple_religion', array(
                        'type' => 'select', 'options' => $religions, 'empty' => true, 'label' => 'Agama istri/suami'
                    ));
                    echo $form->input('couple_work', array('label' => 'Pekerjaan terakhir istri/suami'));
                    echo '</fieldset>';
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php
                        echo $form->end(array('label' => 'Simpan', 'div' => false)) . ' &nbsp; atau &nbsp; ';
                        echo $html->link('Kembali ke daftar keluarga', array('action' => 'index'));
                    ?>
                </td>
            </tr>
        </table>
        </div>
    </div>
</div>
