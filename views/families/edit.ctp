<?php $html->css('admin', 'stylesheet', array('inline' => false));?>
<?php $html->css('colorbox', 'stylesheet', array('inline' => false));?>
<?php $html->script('jquery.colorbox-min', array('inline' => false));?>
<?php $html->script('jquery.carouFredSel-3.2.0', array('inline' => false));?>
<?php echo $html->css('http://code.google.com/apis/maps/documentation/javascript/examples/standard.css', 'stylesheet', array('inline' => false));?>
<?php $html->script('http://maps.google.com/maps/api/js?sensor=false', array('inline' => false));?>
<style type="text/css">
    #map_canvas { height: 400px; width: 600px; }
    <?php if (!empty($this->data['FamilyImage']) ):?>
    .carousel {
        margin-bottom: 5px;
        width: 208px;
    }
    .carousel ul {
        background-color: #efefef;
        margin: 0;
        padding: 0;
        list-style: none;
        display: block;
    }
    .carousel li {
        color: #666;
        text-align: center;
        width: 30px;
        height: 60px;
        padding: 0;
        margin: 6px;
        display: block;
        float: left;
    }
    .carousel li a {
        display: block;
    }
    .carousel li a img {
        border: 2px solid #333;
    }
    .carousel li .delImage,
    .carousel li .setProfile
    {
        font-size: 9px;
    }
    .clearfix {
        float: none;
        clear: both;
    }
    #prev {
        margin-left: 10px;
        font-weight: bold;
    }
    #next {
        float: right;
        margin-right: 10px;
        font-weight: bold;
    }
    .carousel .disabled
    {
        font-weight: normal !important;
        text-decoration: none;
        color: #999;
        cursor: default;
    }
    <?php endif;?>
</style>

<h2><?php echo $title . ' ' . $this->data['Family']['fullname'];?></h2>
<div class="span-24 last">
    <div class="box-content-outer">
        <div class="box-content">
        <?php
            echo $form->create('Family', array(
                'enctype' => 'multipart/form-data'
            ));
        ?>
        <table>
            <tr>
                <td>
                <?php
                    echo $form->input('fullname', array('label' => 'Nama lengkap'));
                    echo $form->input('featured', array(
                        'label' => 'Tampilkan di halaman depan', 'type' => 'checkbox'
                    ));
                    echo $form->input('parent_id', array(
                        'type' => 'select', 'options' => $families, 'empty' => true, 'label' => 'Orang tua'
                    ));
                    echo $form->input('birthday',
                        array('dateFormat' => 'DMY', 'label' => 'Tgl. Lahir', 'minYear' => 1893, 'maxYear' => date('Y'))
                    );
                    echo $form->input('birthplace', array('label' => 'Tempat Lahir'));
                    echo $form->input('rip_date',
                        array('dateFormat' => 'DMY', 'label' => 'Tgl. Wafat', 'minYear' => 1893, 'maxYear' => date('Y'))
                    );
                    echo $form->input('rip_place', array('label' => 'Tempat Wafat'));
                    
                    echo $form->input('religion', array(
                        'type' => 'select', 'options' => $religions, 'empty' => true, 'label' => 'Agama'
                    ));
                    echo $form->input('work', array('label' => 'Pekerjaan terakhir'));
                    
                    echo $form->radio('gender', array('M' => 'Laki-laki', 'F' => 'Perempuan'),
                        array('legend' => 'Jenis Kelamin')
                    );
                    echo $form->input('phone', array('label' => 'Nomor telp'));
                    echo $form->input('email', array('label' => 'Email'));
                    echo $form->input('address', array('label' => 'Alamat', 'type' => 'textarea', 'rows' => 5));
                ?>
                </td>
                <td>
                <?php
                    echo $form->radio('status', array('S' => 'Sendiri', 'M' => 'Menikah', 'D' => 'Cerai'));
                    // couple
                    echo $form->input('married_date',
                        array('dateFormat' => 'DMY', 'label' => 'Tgl. Menikah', 'minYear' => 1893, 'maxYear' => date('Y'))
                    );
                    echo '<fieldset>';
                    echo '<legend>Data suami/istri</legend>';
                    echo $form->input('couple_name', array('label' => 'Nama istri/suami'));
                    echo $form->input('couple_birthday',
                        array('dateFormat' => 'DMY', 'label' => 'Tgl lahir istri/suami', 'minYear' => 1893, 'maxYear' => date('Y'))
                    );
                    echo $form->input('couple_birthplace', array('label' => 'Tempat lahir istri/suami'));
                    echo $form->input('couple_religion', array(
                        'type' => 'select', 'options' => $religions, 'empty' => true, 'label' => 'Agama istri/suami'
                    ));
                    echo $form->input('couple_work', array('label' => 'Pekerjaan terakhir istri/suami'));
                    echo '</fieldset>';
                    
                    // upload gambar
                    echo '<div class="carousel">';
                        if (!empty($this->data['FamilyImage']) ):
                            echo '<ul>';
                            foreach ( $this->data['FamilyImage'] as $cimg):
                                echo '<li><a href="'.$urlImage . $resizedPrefix . $cimg['imagename'] .
                                            '" class="colorbox" rel="' . $this->data['Family']['id'] . '">' .
                                     '<img src="' . $urlImage . $thumbPrefix . $cimg['imagename'] . '" ' .
                                     'width="25" height="25" /></a>' .
                                     (!$cimg['profile'] ? 
                                     $html->link('Profil', array('action' => 'setProfile', $cimg['id'],
                                        $this->data['Family']['id']),
                                        array('class' => 'setProfile')
                                     ) : '') .
                                     $html->link('Hapus', array('action' => 'delImage', $cimg['id'],
                                        $this->data['Family']['id']),
                                        array('class' => 'delImage')
                                     ) . '</li>';
                            endforeach;
                            echo '</ul>';
                            echo '<div class="clearfix"></div>';
                            echo '<a id="prev" href="#">&lt;</a>';
                            echo '<a id="next" href="#">&gt;</a>';
                        endif;
                    echo '</div>';
                    echo '<div id="upload_image">';
                            echo $form->input('imagename', array(
                                'div'=>false, 'label'=>false, 'class'=>'imagename', 'id' => false,
                                'name' => 'data[Family][FamilyImage][imagename][]', 'type' => 'file'
                            ));
                    echo '</div>';
                    echo '<a href="#" id="add_upload_image">Tambah gambar</a>';
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <span class="label">Klik pada peta untuk menandai lokasi<br />
                    atau geser penanda jika ada
                    </span><br />
                    <div id="map_canvas"></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php
                        echo $form->end(array('label' => 'Simpan', 'div' => false)) . ' &nbsp; atau &nbsp; ';
                        echo $html->link('Kembali ke daftar keluarga', array('action' => 'index')) . ' &nbsp; atau &nbsp; ';
                        echo $html->link('Hapus',
                            array('action'=>'delete', $this->data['Family']['id']),
                            array('class'=>'del'),
                            sprintf(__('Yakin ingin menghapus data ', true) . ' %s?', $this->data['Family']['fullname'])
                        );
                    ?>
                </td>
            </tr>
        </table>
        </div>
    </div>
</div>
<script type="text/javascript">
$(function() {
    var map, marker, ll, infowindow;
    var info = "<h3><?php echo $this->data['Family']['fullname'];?></h3>" +
               "<br /><?php echo stripslashes($this->data['Family']['address']);?>" +
               '<br /><?php echo $this->Html->link('Hapus',
                        array('action' => 'unsetLatLng', $this->data['Family']['id']),
                        array('class' => 'unsetLatLng'));?>';
    var urlController = "<?php echo $urlController;?>/updateLatLng/<?php echo $this->data['Family']['id'];?>";
    
    $('#add_upload_image').click(function(e) {
        var cloned = $('.imagename:last', '#upload_image').clone();
        cloned.val('');
        $('#upload_image').append('<br />');
        $('#upload_image').append(cloned);
        
        e.preventDefault();
        return false;
    });
    
    $('.delImage').click(function(e) {
        if ( confirm('Yakin ingin menghapus gambar ini?') ) {
            return true;
        }
        e.preventDefault();
        return false;
    });
    $('.setProfile').click(function(e) {
        if ( confirm('Set photo ini sebagai photo profile?') ) {
            return true;
        }
        e.preventDefault();
        return false;
    });
    
    $('.colorbox').colorbox({
        close: "Tutup"
    });
    
    <?php if (!empty($this->data['FamilyImage']) ):?>
    $('.carousel ul').carouFredSel({
        circular: false,
        infinite: false,
        auto: false,
        prev: "#prev",
        next: "#next"
    });
    <?php endif;?>
    
    function init() {
    <?php if ( !empty($this->data['Family']['lat']) && !empty($this->data['Family']['lng']) ): ?>
        ll = new google.maps.LatLng(<?php echo $this->data['Family']['lat'] . ', ' . $this->data['Family']['lng'];?>);
    <?php else:?> // Kp. Melayu
        ll = new google.maps.LatLng(-6.224216,106.866757);
    <?php endif;?>
    
        var myOptions = {
          zoom: 17,
          center: ll,
          zoomControl: true,
          panControl: false,
          scaleControl: false,
          streetViewControl: false,
          mapTypeControl: false,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        
        google.maps.event.addListener(map, 'click', function(event) {
            if ( marker === undefined ) {
                placeMarker(event.latLng);
                $.post(urlController, {lat: event.latLng.lat(), lng: event.latLng.lng()}, function(resp) {
                    
                });
            }
        });
    <?php if ( !empty($this->data['Family']['lat']) && !empty($this->data['Family']['lng']) ):?>
        placeMarker(ll);
        google.maps.event.trigger(marker, 'click');
    <?php endif;?>
    }
    
    function placeMarker(location) {
        marker = new google.maps.Marker({
            position: location,
            map: map,
            title: "<?php echo $this->data['Family']['fullname'];?>",
            draggable:true,
            animation: google.maps.Animation.DROP
        });
        map.setCenter(location);
        
        google.maps.event.addListener(marker, 'click', function() {
            infowindow = new google.maps.InfoWindow({
                content: info,
                size: new google.maps.Size(50,200)
            });
            infowindow.open(map, marker);
        });
        google.maps.event.addListener(marker, 'dragend', function() {
            map.setCenter(marker.position);
            $.post(urlController, {lat: marker.position.lat(), lng: marker.position.lng()}, function(resp) {
                
            });
        });
    }
    
    init();
    $('.unsetLatLng').live('click', function(e) {
        marker.setVisible(false);
        marker = undefined;
        infowindow.close();
        
        var href = $(this).attr('href');
        $.get(href, function(resp) {
        });
        e.preventDefault();
        return false;
    });
});
</script>
