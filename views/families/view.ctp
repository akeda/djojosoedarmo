<?php $html->css('profil', 'stylesheet', array('inline' => false));?>
<h2><?php echo $title . ' ' . $record['Family']['fullname'];?></h2>
<div class="span-24 last">
    <div id="profile" class="box-content-outer">
        <ul class="box-content" id="profile-content">
            <li>
                <div class="people-pict">
                    <?php if ( !empty($record['Family']['filename']) ):?>
                        <?php 
                            echo '<img src="' . $urlImage . $thumbPrefix . $record['Family']['filename'] . 
                                 '" width="180" height="180" />';
                        ?>
                    <?php else:?>
                        <?php echo $html->image('no_photo.jpg');?>
                    <?php endif;?>
                    <div>
                        <table class="people-info">
                            <tr>
                                <td class="label">Facebook</td>
                                <td class="val">sdadasd</td>
                            </tr>
                            <tr>
                                <td class="label">Email</td>
                                <td class="val">sdadasd</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="people-desc">
                    <h3><?php echo $record['Family']['fullname'];?></h3>
                    <div class="information">
                        <h4>Mengupdate profil</h4>
                        <p>
                        Jika Anda adalah pemilik profil di halaman ini maka Anda dapat
                        menyunting halaman ini dengan login terlebih dahulu.
                        </p>
                    </div>
                    <p>
                        <?php
                            // birth
                            echo 'Lahir di ' . $record['Family']['birthplace'] . ' pada tanggal ' .
                                  $time->format('d/m/Y', $record['Family']['birthday']) . '. ';
                            
                            // work
                            if ( !empty($record['Family']['work']) ):
                                echo 'Pekerjaan terakhir adalah ' . $record['Family']['work'] . '. ';
                            endif;
                            
                            // couple
                            if ( $record['Family']['status'] == 'M' ):
                                echo 'Menikah dengan ' . $record['Family']['couple_name'] . ', ';
                                echo $record['Family']['couple_work'] . ', pada tanggal ';
                                echo $time->format('d/m/Y', $record['Family']['married_date']) . '.';
                            endif;
                            
                            // children
                            
                            // work
                        ?>
                    <div class="people-desc-detail">
                        <div class="label">Email:</div>
                        <div class="val">
                            <?php echo $record['Family']['email'] ? $record['Family']['email'] : '-';?>
                        </div>
                    </div>
                    <div class="people-desc-detail">
                        <div class="label">Alamat:</div>
                        <div class="val">
                            <?php echo $record['Family']['address'];?>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <h3 id="commentinfo">Ada 3 komentar untuk Akeda Bagus Jully Setiasgi S.Kom</h3>
                <ol id="comments">
                    <li>
                        <div class="comment">
                            <p>asdasdasd askdjasd</p>
                        </div>
                        <div class="comment_author">
                            <span class="name">Komentar dari <a href="#">Dibyo Sugeng</a></span>,
                            <span class="timestamp">01/01/2011 03:55</span>
                        </div>
                    </li>
                    <li class="alt">
                        <div class="comment">
                            <p>asdasdasd askdjasd</p>
                        </div>
                        <div class="comment_author">
                            <span class="name">Komentar dari <a href="#">Dibyo Sugeng</a></span>,
                            <span class="timestamp">01/01/2011 03:55</span>
                        </div>
                    </li>
                    <li>
                        <div class="comment">
                            <p>asdasdasd askdjasd</p>
                        </div>
                        <div class="comment_author">
                            <span class="name">Komentar dari <a href="#">Dibyo Sugeng</a></span>,
                            <span class="timestamp">01/01/2011 03:55</span>
                        </div>
                    </li>
                </ol>
                <div id="givecomment">
                    <h3>Beri komentar</h3>
                    <div class="information">
                        <h4>Mengirimkan komentar</h4>
                        <p>
                        Untuk dapat memberi komentar ke halaman ini,
                        Anda perlu login terlebih dahulu.
                        </p>
                    </div>
                    <div class="row">
                        <label>Komentar :</label>
                        <textarea></textarea>
                        <input type="submit" value="Kirim" />
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
