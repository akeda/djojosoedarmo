<?php
class User extends AppModel {
	var $name = 'User';
	var $displayField = 'name';
	var $validate = array(
		'type' => array(
			'inlist' => array(
				'rule' => array('inlist', array('member', 'admin')),
				'message' => 'Pilih member atau admin',
				'allowEmpty' => true,
				'required' => false,
                'on' => 'create'
			),
		),
        'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Wajib diisi',
				'allowEmpty' => false,
				'required' => true
			),
		),
        'password' => array(
            'notEmpty' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => 'notEmpty',
                'message' => 'Wajib diisi'
            ),
            'confirm' => array(
                'rule' => 'vPassword',
                'message' => 'Password tidak sama'
            )
        ),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				'message' => 'Email tidak valid',
				'allowEmpty' => false,
				'required' => true
			),
		),
	);

    function vPassword($field) {
        if ( isset($this->data[$this->alias]['password_confirm']) && ($this->data[$this->alias]['password_confirm'] != $field['password']) ) {
            $this->validationErrors['password_confirm'] = 'Password tidak sama';
            return false;
        }
        
        return true;
    }
    
    function hashPasswords($data) {
        if (isset($data['User']['password'])) {
            $data['User']['password'] = sha1($data['User']['password']);
            return $data;
        }
        return $data;
    }
}
?>
