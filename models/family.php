<?php
class Family extends AppModel {
    var $actsAs = array('Tree');
    var $displayField = 'fullname';
    
    var $hasMany = array(
        'FamilyImage'
    );
    
    function beforeDelete() {
        if ( $this->childCount() ) {
            return false;
        }
        
        return true;
    }
}
