<?php
class AppModel extends Model {
    
    function unique($data, $name){
        $this->reqursive = -1;
        $found = $this->find($this->name.".$name='".$data[$name]."'");
        $same = isset($this->id) && $found[$this->name]['id'] == $this->id;
        
        return !$found || $found && $same;
    }
    
    function checkByField() {
        //Get the name of the table
        $db =& ConnectionManager::getDataSource($this->useDbConfig);
        $tableName = $db->fullTableName($this, false);
        
        // cek if field created_by and modified_by is exists in current table
        $result = $this->query("SHOW COLUMNS FROM {$tableName} LIKE '%ed_by'");
        
        if ( isset( $result[0]['COLUMNS']['Type'] ) || isset( $result[0][0]['Type'] ) ) {
            return true;
        } else {
            return false;
        }
    }
    
    function afterSave() {
        $this->__clearCachedData('_cached_data', 'model', '');
    }
    
    function __clearCachedData() {
        $folder = new Folder( CACHE );
        $cache =& Cache::getInstance();
        
        // configure cache object
        $cache->config('default', array('engine' => 'File', 'path' => CACHE . DS . 'models', 'prefix' => '', 'duration' => 86400));
        
        // cd to path of caches, relative to CACHE
        $folder->cd( CACHE . DS . 'models' );
        
        // find all *_cached_data files
        $caches = array();
        $caches = $folder->find('.*._cached_data');
        
        if ( count($caches) > 0 ) {         
            foreach ( $caches as $cachedData ) {
                @unlink( CACHE . 'models' . DS . $cachedData );
            }
        }
        
    }
    
    function __refreshMenu() {
        //echo CACHE . 'views' . DS . 'element_group_id_' . $this->data['Auth']['User']['group_id'] .  '_menu_sidebar';
        @unlink( CACHE . 'views' . DS . 'element_group_id_' . $this->data['Auth']['User']['group_id'] .  '_menu_sidebar' );
        return true;
    }

    function findDistinct($conditions = null, $fields = null){
        $db =& ConnectionManager::getDataSource($this->useDbConfig);

        $str = 'DISTINCT ';
        if (!is_array($fields)){
            $str .= '`' . $fields . '`';
        }else{
            foreach ($fields as $field){
                $str .= '`' . $field . '`, ';
            }
            $str = substr($str, 0, -2);   
        }
        $queryData = array(
                    'conditions'   => $conditions,
                    'fields'       => $str,
                    );
        $data = $db->read($this, $queryData, false);

        return $data;      
    }

}
?>
