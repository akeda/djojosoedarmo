<?php
class PagesController extends AppController {
	var $uses = array();
    
    function home() {
        App::import('Model', 'Family');
        $this->Family = new Family;
        
        $families = $this->Family->find('all');
        $this->set('families', json_encode($families));
    }
    
    function intro() {
    }
}
