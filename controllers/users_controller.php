<?php
class UsersController extends AppController {
    function beforeFilter() {
        $this->Auth->authenticate = ClassRegistry::init('User');
        
        $this->Auth->allow('register', 'login');
        if ( $this->Auth->user('type') == 'member' ) {
            $this->Auth->deny('index', 'add', 'delete', 'edit');
            $this->Auth->allow('profile', 'view', 'logout');
        }
        
        $this->Auth->fields = array(
            'username' => 'email',
            'password' => 'password'
        );
        $this->Auth->autoRedirect = false;
        parent::beforeFilter();
    }
    
	function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid user', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('user', $this->User->read(null, $id));
	}
    
	function add() {
		if (!empty($this->data)) {
            if ( !empty($this->data['User']['password_confirm']) ) {
                $this->data['User']['password_confirm'] = $this->Auth->password($this->data['User']['password_confirm']);
                $this->User->create();
                if ($this->User->save($this->data)) {
                    $this->Session->setFlash(__('User berhasil disimpan', true));
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('User tidak berhasil disimpan. Coba lagi!', true));
                }
            } else {
                $this->User->validationErrors['password_confirm'] = 'Wajib diisi';
                unset($this->data['User']['password']);
                unset($this->data['User']['password_confirm']);
                $this->Session->setFlash(__('Tidak behasil mendaftar. Coba lagi!', true));
            }
		}
	}
    
    function register() {
        if ( $this->Auth->user('id') ) {
            $this->redirect(array('action' => 'profile'));
        }
        
		if (!empty($this->data)) {
            if ( !empty($this->data['User']['password_confirm']) ) {
                $this->data['User']['password_confirm'] = $this->Auth->password($this->data['User']['password_confirm']);
                $this->User->create();
                if ($this->User->save($this->data)) {
                    $this->Auth->login($this->data);
                    $this->Session->setFlash(__('Behasil terdaftar', true));
                    $url = $this->Cookie->read('Previous.url');
                    if ( substr($url, 0, 12)  == '/users/login' ) {
                        $url = '/users/profile';
                    }
                    $this->redirect($url);
                } else {
                    unset($this->data['User']['password']);
                    unset($this->data['User']['password_confirm']);
                    $this->Session->setFlash(__('Tidak behasil mendaftar. Coba lagi!', true));
                }
            } else {
                $this->User->validationErrors['password_confirm'] = 'Wajib diisi';
                unset($this->data['User']['password']);
                unset($this->data['User']['password_confirm']);
                $this->Session->setFlash(__('Tidak behasil mendaftar. Coba lagi!', true));
            }
		}
	}
    
    function login() {
        if ($this->Auth->user()) {
            // save last login when login
            if (!empty($this->data['User']['username'])) {
                $this->User->id = $this->Auth->user('id');
            }
            
            $redirect = $this->Session->read('Auth.redirect');
            
            if ( is_null($redirect) || empty($redirect) || $redirect === '/users/logout' ) {
			    $this->Auth->loginRedirect = array('/pages/home');
                $this->redirect('/');
            }
            $this->redirect($this->Auth->redirect());
		}
    }
    
    function logout() {
        $this->redirect($this->Auth->logout());
    }

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid user', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->User->save($this->data)) {
				$this->Session->setFlash(__('User berhasil disimpan.', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('User tidak berhasil disimpan. Coba lagi!', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->User->read(null, $id);
		}
	}
    
    function profile() {
        $id = $this->Auth->user('id');
        if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid user', true), 'error');
			$this->redirect(array('controller' => 'events', 'action' => 'index'));
		}
        
        $this->User->id = $id;
		if (!empty($this->data)) {
			if ( !empty($this->data['User']['password_confirm']) ) {
                $this->data['User']['password_confirm'] = sha1($this->data['User']['password_confirm']);
                if ($this->User->save($this->data, array('fieldList' => array('password', 'password_confirm')))) {
                    $this->Session->setFlash(__('Password berhasil diupdate.', true), 'success');
                } else {
                    $this->Session->setFlash(__('Password tidak berhasil diupdate. Coba lagi!', true), 'error');
                }
                unset($this->data['User']['password']);
                unset($this->data['User']['password_confirm']);
            } else {
                $this->User->validationErrors['password_confirm'] = 'Wajib diisi';
                unset($this->data['User']['password']);
                unset($this->data['User']['password_confirm']);
                $this->Session->setFlash(__('Password tidak berhasil diupdate. Coba lagi!', true), 'error');
            }
		}
		if (empty($this->data)) {
			$this->data = $this->User->read(null, $id);
		}
    }

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid user', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->User->delete($id)) {
			$this->Session->setFlash(__('User berhasil dihapus', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('User tidak berhasil dihapus', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>
