<?php
class FamiliesController extends AppController {    
    function index() {
        $this->__setAdditionals();
        $this->set('title', 'Daftar biodata keluarga');
        
        $alphabet = 'A';
        $conditions = array();
        if ( isset($this->params['url']['alphabet']) && !empty($this->params['url']['alphabet']) ) {
            $alphabet = $this->params['url']['alphabet'];
        }
        $conditions['Family.fullname LIKE'] = $alphabet . '%';
        $order = 'Family.fullname ASC';
        
        // on search
        $fullname = '';
        if ( isset($this->params['url']['fullname']) && !empty($this->params['url']['fullname']) ) {
            $fullname = $this->params['url']['fullname'];
            $conditions['Family.fullname LIKE'] = "%$fullname%";
            $alphabet = '';
        }
        
        $records = $this->Family->find('all', compact('conditions', 'order'));
        $this->set('records', $records);
        $this->set('current_alphabet', $alphabet);
        $this->set('fullname', $fullname);
        $this->set('searchfield_default', 'Ketik nama anggota keluarga dan tekan enter...');
    }

    function add() {
        $this->__setAdditionals();
        $this->set('title', 'Tambah informasi keluarga');

        if ( !empty($this->data) ) {
            $this->Family->create();
            if ( $this->Family->save($this->data) ) {
                $this->Session->setFlash('Berhasil disimpan', 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Tidak berhasil disimpan', 'success');
            }
        }
    }

    function edit($id) {
        $this->__setAdditionals();
        $this->set('title', 'Edit informasi keluarga');
        
        $complete = true;
        $error_msg = '';
        if ( !empty($this->data) ) {
            $this->Family->id = $id;
            if ( $this->Family->save($this->data) ) {
                if ( isset($this->data['Family']['FamilyImage']['imagename']['name']) && 
                     !empty($this->data['Family']['FamilyImage']['imagename']['name']) ) {
                    $imgname = $this->data['Family']['FamilyImage']['imagename']['name'];
                    $tmps = $this->data['Family']['FamilyImage']['imagename']['tmp_name'];
                    $errors = $this->data['Family']['FamilyImage']['imagename']['error'];
                    
                    foreach ($imgname as $key => $name) {
                        if ( ($s = $this->__handleUploadImage($errors[$key], $tmps[$key], $name)) !== FALSE ) {
                            $saved_img = array(
                                'imagename' => $s, 'family_id' => $this->Family->id
                            );
                            $this->Family->FamilyImage->create();
                            $this->Family->FamilyImage->save($saved_img);
                        } else {
                            $error_msg = implode(', ', $this->errorUpload);
                            $complete = false;
                            break;
                        }
                    }
                }
                
                $this->Session->setFlash('Berhasil disimpan', 'success');
                if ( !$complete ) {
                    $this->Session->setFlash($error_msg, 'error');
                }
                
                $this->redirect(array('action' => 'edit', $id));
            } else {
                $this->Session->setFlash('Tidak berhasil disimpan', 'success');
            }
        }
        
        if (empty($this->data)) {
			$this->data = $this->Family->find('first', array(
                'conditions' => array('Family.id' => $id)
            ));
            
            if (!$this->data) {
                $this->Session->setFlash(__('Invalid parameter', true), 'error');
                $this->redirect(array('action' => 'index'));
            }
		}
        $this->set('urlController', $this->__pathToController());
    }
    
    function view($id) {
        $this->__setAdditionals();
        $record = $this->Family->find('first', array(
            'conditions' => array('Family.id' => $id)
        ));
        
        if (!$record) {
            $this->Session->setFlash(__('Invalid parameter', true), 'error');
            $this->redirect(array('action' => 'index'));
        }
        
        $this->set('record', $record);
        $this->set('title', 'Biodata');
        $this->set('urlController', $this->__pathToController());
        
    }
    
    function delete($id) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid parameter', true), 'error');
            $this->redirect(array('action' => 'index'));
        }
        
        if ( $this->Family->delete($id) ) {
            $this->Session->setFlash(__('Berhasil dihapus', true), 'success');
        } else {
            $this->Session->setFlash(__('Tidak berhasil dihapus', true), 'error');
        }
        $this->redirect(array('action' => 'index'));
    }
    
    function updateLatLng($id) {
        $this->autoRender = false;
        Configure::write('debug', 0);
        
        $data = array(
            'lat' => $this->params['form']['lat'],
            'lng' => $this->params['form']['lng']
        );
        $result = 0;
        $this->Family->id = $id;
        $this->Family->save($data, array(
            'validate' => false, 'callback' => false,
            'fieldList' => array('lat', 'lng')
        ));
    }
    
    function delImage($id, $family_id) {
        $this->Session->setFlash('Gambar tidak berhasil terhapus', 'error');
        
        $this->Family->FamilyImage->id = $id;
        $imagename = $this->Family->FamilyImage->field($this->fieldImage);
        if ( $this->Family->FamilyImage->delete($id) ) {
            @unlink($this->pathPrefix . DS . $imagename);
            @unlink($this->pathPrefix . DS . $this->resizedPrefix . $imagename);
            @unlink($this->pathPrefix . DS . $this->thumbPrefix . $imagename);
            @unlink($this->pathPrefix . DS . $this->iconPrefix . $imagename);
            $this->Session->setFlash('Gambar berhasil terhapus', 'success');
        }
        $this->redirect(array('action' => 'edit', $family_id));
    }
    
    function setProfile($id, $family_id) {
        $this->Session->setFlash('Gambar tidak berhasil di set sebagai profile', 'error');
        
        $this->Family->FamilyImage->id = $id;
        $imagename = $this->Family->FamilyImage->field($this->fieldImage);
        $this->Family->FamilyImage->updateAll(
                array('FamilyImage.profile' => 0),
                array('FamilyImage.family_id' => $family_id)
        );
        
        if ( $this->Family->FamilyImage->save(
                array('profile' => 1),
                array('fieldList' => array('profile'))
        ))
        {
            
            $this->Family->id = $family_id;
            $this->Family->save(
                array('filename' => $imagename),
                array('fieldList' => array('filename'))
            );
            
            $this->Session->setFlash('Gambar berhasil di set sebagai profile', 'success');
        }
        
        $this->redirect(array('action' => 'edit', $family_id));
    }
    
    function unsetLatLng($id) {
        $this->autoRender = false;
        Configure::write('debug', 0);
        
        $this->Family->id = $id;
        $data = array(
            'lat' => null,
            'lng' => null
        );
        $this->Family->save($data, array(
            'validate' => false, 'callback' => false,
            'fieldList' => array('lat', 'lng')
        ));
    }
    
    function getFeatured() {
        $records = array();
        if ( isset($this->params['requested']) ) {
            $records = $this->Family->find('all', array(
                'conditions' => array(
                    'featured' => 1
                ),
                'limit' => 6
            ));
        }
        return $records;
    }
    
    function getBirthday() {
        $today = date('Y-m-d');
        $records = array();
        if ( isset($this->params['requested']) ) {
            $records = $this->Family->find('list', array(
                'conditions' => array(
                    'birthday LIKE' => '%' . substr($today, 5)
                ),
                'fields' => array('id', 'fullname')
            ));
        }
        return $records;
    }
    
    function __handleUploadImage($error_code, $tmp_name, $image_name) {
        $error = array();
        $possible_errors = array(
            1 => 'php.ini max file exceeded',
            2 => 'html form max file size exceeded',
            3 => 'file upload was only partial',
            4 => 'no file was attached'
        );
        
        // check for PHP's built-in uploading errors
        if ( $error_code > 0 ) {
            $error[] = $possible_errors[$error_code];
        }
        
        // check that the file we're working on really was the subject of an HTTP upload
        if ( !@is_uploaded_file($tmp_name) ) {
            $error[] = 'Not an HTTP upload';
        }
        
        // check if uploaded file is in fact an image
        if ( !@getimagesize($tmp_name) ) {
            $error[] = 'Only image uploads are allowed';
        }
        
        // check if our target directory is writeable
        if ( !is_writable($this->pathImage) ) {
            $error[] = 'Target upload is not writeable.';
        }
        
        if ( empty($error) ) {
            // Make a unique filename for the uploaded files and check it is not already
            // taken.
            
            // first check if we could use the original image name
            $image_path = $this->pathImage . $image_name;
            if ( file_exists($image_path) ) {
                $image_name = time() . '_' . $image_name;
                
                // uses time and iterate until it found unique name
                $image_path = $this->pathImage . $image_name;
                if ( file_exists($image_path) ) {
                    $counter = 1;
                    while ( file_exists($image_path = $this->pathImage . $counter . '_' . $image_name ) ) {
                        $counter++;
                    }
                    $image_name = $counter . '_' . $image_name;
                }
            }
            
            // error moving uploaded file
            if ( !@move_uploaded_file($tmp_name, $image_path) ) {
                $this->errorUpload = 'Receiving directory insufficient permission';
                
                return false;
            }
            
            // resizing
            if ( $this->__resizeImage($image_name) ) {
                return $image_name;
            } else {
                $this->errorUpload = $error;
                return false;
            }
            
        } else {
            $this->errorUpload = $error;
            return false;
        }
    }
    
    function __setAdditionals() {
        $families = $this->Family->generatetreelist();
        $this->set('families', $families);
        
        $religions = array(
            'M' => 'Islam', 'C' => 'Kristen Katolik', 'P' => 'Kristen Protestan',
            'B' => 'Buddha', 'H' => 'Hindu'
        );
        $this->set('religions', $religions);
        
        $alphabets = array();
        for ($i = 65; $i <= 90; $i++) {
            $alphabets[] = chr($i);
        }
        $this->set('alphabets', $alphabets);
        
        // images related
        $this->set('urlImage', $this->urlImage);
        $this->set('resizedPrefix', $this->resizedPrefix);
        $this->set('thumbPrefix', $this->thumbPrefix);
    }
}
